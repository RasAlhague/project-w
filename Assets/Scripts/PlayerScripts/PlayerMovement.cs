﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectW.PlayerScripts
{
	public class PlayerMovement : Movement.MovementBase
	{
		// Use this for initialization
		void Start()
		{
			Cursor.lockState = CursorLockMode.Locked;
		}

		// Update is called once per frame
		void Update()
		{
			float translation = Input.GetAxis("Vertical") * Speed;
			float jump = Input.GetAxis("Jump") * Speed;
			float straffe = Input.GetAxis("Horizontal") * Speed;
			translation *= Time.deltaTime;
			straffe *= Time.deltaTime;
			jump *= Time.deltaTime;

			transform.Translate(straffe, jump, translation);

			if (Input.GetKeyDown("escape"))
			{
				Cursor.lockState = CursorLockMode.None;
			}
		}
	}

}
