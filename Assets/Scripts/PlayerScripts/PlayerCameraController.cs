﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectW.PlayerScripts
{
	public class PlayerCameraController : MonoBehaviour
	{
		private Vector2 mouseLook;
		private Vector2 smoothV;
		private GameObject character;

		public float Sensitivity = 5.0f;
		public float Smoothing = 2.0f;


		// Use this for initialization
		void Start()
		{
			character = transform.parent.gameObject;
		}

		// Update is called once per frame
		void Update()
		{
			var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

			md = Vector2.Scale(md, new Vector2(Sensitivity * Smoothing, Sensitivity * Smoothing));
			smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / Smoothing);
			smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / Smoothing);
			mouseLook += smoothV;

			transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
			character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
		}
	}

}
